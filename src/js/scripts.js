import '../scss/styles.scss';


window.addEventListener('scroll', function() {
    window.scrollY > 1 ? document.querySelector('.header').classList.add('header__sticky') : document.querySelector('.header').classList.remove('header__sticky');
});

let thirdTabContent = document.querySelector('.tab-content:nth-child(3)');
let dragImg = document.querySelector('.drag-picture');
let firstCoord = getCoords(dragImg);
console.log(firstCoord);

dragImg.onmousedown = function(e) {
    let coords = getCoords(dragImg);
    let shiftX = e.pageX - coords.left;
    let shiftY = e.pageY - coords.top;
    // dragImg.style.zIndex = 1000;

    function moveImg(e) {
        dragImg.style.left = e.pageX - shiftX + "px";
        dragImg.style.top = e.pageY - shiftY + "px";
    }

    thirdTabContent.onmousemove = function(e) {
        moveImg(e);
    };

    thirdTabContent.onmouseout = function() {
        thirdTabContent.onmousemove = null;
        dragImg.onmouseup = null;
    };

    dragImg.onmouseup = function() {
        thirdTabContent.onmousemove = null;
        dragImg.onmouseup = null;
    };
};

dragImg.ondragstart = function() {
    return false;
};

function getCoords(elem) {
    return {
        top: elem.offsetTop,
        left: elem.offsetLeft
    };
}

let lastTabContent = document.querySelector('.tab-content:nth-child(4)');
let hoverImg = document.querySelector('.hover-picture');

lastTabContent.onmousemove = function (e) {
    e = e || window.e;

    if (e.offsetX < 170 && e.offsetY < 441) {
        hoverImg.style.top = '50%';
        hoverImg.style.right = 'initial';
        hoverImg.style.bottom = 'initial';
        hoverImg.style.left = '0';
        hoverImg.style.transform = 'translateX(0) translateY(-50%)';
    } else if(e.offsetX < 928 && e.offsetY < 200) {
        hoverImg.style.top = '0';
        hoverImg.style.right = 'initial';
        hoverImg.style.bottom = 'initial';
        hoverImg.style.left = '50%';
        hoverImg.style.transform = 'translateX(-50%) translateY(0)';
    } else if(e.offsetX > 820 && e.offsetY < 441) {
        hoverImg.style.top = '50%';
        hoverImg.style.right = '0';
        hoverImg.style.bottom = 'initial';
        hoverImg.style.left = 'initial';
        hoverImg.style.transform = 'translateX(0) translateY(-50%)';
    } else if(e.offsetX < 928 && e.offsetY > 350) {
        hoverImg.style.top = 'initial';
        hoverImg.style.right = 'initial';
        hoverImg.style.bottom = '0';
        hoverImg.style.left = '50%';
        hoverImg.style.transform = 'translateX(-50%) translateY(0)';
    } else {
        hoverImg.style.top = '50%';
        hoverImg.style.right = 'initial';
        hoverImg.style.bottom = 'initial';
        hoverImg.style.left = '50%';
        hoverImg.style.transform = 'translateX(-50%) translateY(-50%)';
    }
}

lastTabContent.onmouseout = function () {
    hoverImg.style.top = '50%';
    hoverImg.style.right = 'initial';
    hoverImg.style.bottom = 'initial';
    hoverImg.style.left = '50%';
    hoverImg.style.transform = 'translateX(-50%) translateY(-50%)';
}

